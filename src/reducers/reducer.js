import CalculatorModel from "../model/calculatormodel";

const reducer = (state =
    {
        beers: {
            range: [0, 6],
            listBeers: []
        },
        heroes: {
            heroesList: [],
            heroForm: {
                name: '',
                description: ''
            }
        },
        apod: {
            date: new Date()
        },
        calculator: new CalculatorModel(),
        sw: {
            active: 1
        }
    },
    action) => {
    switch (action.type) {
        case "UPDATE_RANGE":
            state.beers.range = action.payload.range
            return state;
        case "UPDATE_LIST_BEERS":
            state.beers.listBeers = action.payload.beers
            return state;
        case "ADD_HERO":
            state.heroes.heroesList = [...state.heroes.heroesList, { ...action.payload.hero }]
            return state;
        case "UPDATE_HERO_FORM":
            state.heroes.heroForm = { ...action.payload.hero }
            return state;
        case "UPDATE_DATE":
            state.apod.date = action.payload.date;
            return state;
        case "UPDATE_CALCULATOR":
            state.calculator = action.payload.calculator;
            return state;
        case "UPDATE_SW":
            state.sw = action.payload.data;
            return state;

        default:
            return state;
    }
};

export default reducer;