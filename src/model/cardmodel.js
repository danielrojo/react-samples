export class CardModel {
    constructor(json) {
        this.question = json.question;
        this.answers = json.incorrect_answers;
        this.answers = [...this.answers, json.correct_answer];
        this.correctAnswer = json.correct_answer;
        switch (json.difficulty) {
            case "hard":
                this.difficulty = 3;
                break;
            case "medium":
                this.difficulty = 2;
                break;
            case "easy":
                this.difficulty = 1;
                break;
            default:
                this.difficulty = undefined
                break;
        }
        this.responded = false;
        this.correct = undefined;
        this.indexResponse = -1;
        this.shuffleArray();
    }

    getDifficulty() {
        switch (this.difficulty) {
            case 3:
                return "Hard"
            case 2:
                return "Medium"
            case 1:
                return "Easy"

            default:
                return ""
        }
    }

    shuffleArray() {
        for (var i = this.answers.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = this.answers[i];
            this.answers[i] = this.answers[j];
            this.answers[j] = temp;
        }
    }
}

export default CardModel;