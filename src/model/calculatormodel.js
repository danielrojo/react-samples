export class CalculatorModel {

    constructor() {
        this.calculatorState = 0
        this.firstFigure = 0
        this.secondFigure = 0
        this.result = 0
        this.operator = ''
        this.display = ''
    }
}

export default CalculatorModel;