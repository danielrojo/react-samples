import React, { Component } from 'react'
import Display from './display';
import Keyboard from './keyboard';

const INIT = 0;
const FIRST_FIGURE = 1;
const SECOND_FIGURE = 2;
const RESULT = 3;

export class Calculator extends Component {

    constructor(props) {
        super(props)

        this.calculatorModel = this.props.data;

        this.state = {
            display: this.props.data.display
        }
    }

    updateCalculatorData(display) {
        this.setState({ display });
        this.calculatorModel.display = display;
        this.props.updateCalculator(this.calculatorModel);
    }

    handleNumber(number) {
        switch (this.calculatorModel.calculatorState) {
            case INIT:
                this.calculatorModel.firstFigure = number;
                this.calculatorModel.calculatorState = FIRST_FIGURE;
                this.updateCalculatorData(this.state.display + String(number));
                break;
            case FIRST_FIGURE:
                this.calculatorModel.firstFigure = this.calculatorModel.firstFigure * 10 + number;
                this.updateCalculatorData(this.state.display + String(number));
                break;
            case SECOND_FIGURE:
                this.calculatorModel.secondFigure = this.calculatorModel.secondFigure * 10 + number;
                this.updateCalculatorData(this.state.display + String(number));
                break;
            case RESULT:
                this.calculatorModel.firstFigure = number;
                this.calculatorModel.result = 0;
                this.calculatorModel.secondFigure = 0;
                this.calculatorModel.operator = '';
                this.calculatorModel.calculatorState = FIRST_FIGURE;
                this.updateCalculatorData(String(number));
                break;

            default:
                break;
        }
    }

    resolve() {
        switch (this.calculatorModel.operator) {
            case '+':
                return this.calculatorModel.firstFigure + this.calculatorModel.secondFigure;
            case '-':
                return this.calculatorModel.firstFigure - this.calculatorModel.secondFigure;
            case '*':
                return this.calculatorModel.firstFigure * this.calculatorModel.secondFigure;
            case '/':
                return this.calculatorModel.firstFigure / this.calculatorModel.secondFigure;

            default:
                break;
        }
    }

    handleSymbol(symbol) {
        switch (this.calculatorModel.calculatorState) {
            case INIT:

                break;
            case FIRST_FIGURE:
                if (symbol === '+' || symbol === '-' || symbol === '/' || symbol === '*') {
                    this.calculatorModel.operator = symbol;
                    this.calculatorModel.calculatorState = SECOND_FIGURE;
                    this.updateCalculatorData(this.state.display + symbol);
                }
                break;
            case SECOND_FIGURE:
                if (symbol === '=') {
                    this.calculatorModel.result = this.resolve();
                    this.calculatorModel.calculatorState = RESULT;
                    this.updateCalculatorData(this.state.display + symbol + this.calculatorModel.result);
                }
                break;
            case RESULT:
                if (symbol === '+' || symbol === '-' || symbol === '/' || symbol === '*') {
                    this.calculatorModel.firstFigure = this.calculatorModel.result;
                    this.calculatorModel.operator = symbol;
                    this.calculatorModel.secondFigure = 0;
                    this.calculatorModel.result = 0;
                    this.calculatorModel.calculatorState = SECOND_FIGURE;
                    this.updateCalculatorData(String(this.calculatorModel.firstFigure) + symbol);
                }
                break;

            default:
                break;
        }

    }

    handleClick(value) {
        if (typeof value === 'number') {
            this.handleNumber(value);
        } else if (typeof value === 'string') {
            this.handleSymbol(value);
        }

    }

    render() {
        return (
            <div class="container">
                <table border="1">
                    <tbody>
                        <Display input={this.state.display}/>
                        <Keyboard output={(v)=>{this.handleClick(v)}}/>
                   </tbody>
                </table>
            </div>
        )
    }
}

export default Calculator
