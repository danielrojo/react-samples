import React, { Component } from 'react'
import HeroesList from './heroeslist';
import HeroForm from './heroform';

export class Heroes extends Component {

    constructor(props) {
        super(props)

        this.state = {
            heroes: this.props.heroes
        }
    }

    handleNewHero(hero) {
        let tmp = this.state.heroes;
        tmp = [...tmp, {...hero}];
        this.setState({ heroes: tmp });
        this.props.addHero({...hero});
    }

    handleFormChange(hero) {
        this.props.updateHeroForm({...hero});
    }

    removeHero(index) {
        let tmp = this.state.heroes;
        tmp.splice(index,1);
        this.setState({heroes: tmp});
    }

    render() {

        return (
            <div class="container">
                <HeroForm addHero={(hero)=>{this.handleNewHero(hero)}} hero={this.props.heroForm} formChange={(hero)=>this.handleFormChange(hero)}/>
                <HeroesList heroes={this.state.heroes} remove={(i)=>{this.removeHero(i)}}/>
            </div>
        )
    }
}

export default Heroes
