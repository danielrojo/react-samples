import React, { Component } from 'react'

export class HeroesList extends Component {
    constructor(props) {
        super(props)

        this.state = {

        }
    }

    removeHero(index) {
        this.props.remove(index);
    }

    render() {
        let elements = <div></div>;
        elements = this.props.heroes.map((hero, index) =>
            <li class="list-group-item" key={index}>
                <strong>{hero.name}</strong>
                <br />
                <p>{hero.description}</p>
                <button type="button" class="btn btn-danger" onClick={() => { this.removeHero(index) }}>Borrar</button>
            </li>
        )
        return (
            <ul class="list-group">
                {elements}
            </ul>
        )
    }
}

export default HeroesList
