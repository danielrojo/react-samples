import React, { Component } from 'react'

export class HeroForm extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
            newHero: this.props.hero,
            showAlert: 'alert alert-danger alert-dismissible fade'
        }
    }

    handleChange(event) {
        let tmp = this.state.newHero;
        if (event.target.name === 'name') {
            tmp.name = event.target.value;
            this.setState({ newHero: tmp });
            if(event.target.validity.valid){
                this.setState({showAlert: 'alert alert-danger alert-dismissible fade'})
            }else {
                this.setState({showAlert: 'alert alert-danger alert-dismissible fade show'})
            }
        } else if (event.target.name === 'description') {
            tmp.description = event.target.value;
            this.setState({ newHero: tmp });
        }
        this.props.formChange(tmp);
    }

    handleClick() {
        this.props.addHero(this.state.newHero);
        this.setState({ newHero: { name: '', description: '' } });
        this.props.formChange({ name: '', description: '' });
    }
    
    render() {
        return (
                <div class="form-group">
                    <label >Nombre</label>
                    <input type="text"
                        class="form-control" name="name" id="" aria-describedby="helpId" placeholder=""
                        value={this.state.newHero.name} onChange={(event) => { this.handleChange(event) }} required />
                        <div class={this.state.showAlert} role="alert">
                            <strong>El nombre no puede estar vacío</strong>
                        </div>
                    <label >Descripción</label>
                    <input type="text"
                        class="form-control" name="description" id="" aria-describedby="helpId" placeholder=""
                        value={this.state.newHero.description} onChange={(event) => { this.handleChange(event) }} />
                    <button type="button" class="btn btn-primary mt-1" onClick={() => { this.handleClick() }}>añadir</button>
                </div>
        )
    }
}

export default HeroForm
