import { TableHead } from '@material-ui/core';
import React, { useState, useEffect } from 'react'
import he from 'he';

function Card({ data, onResponse }) {

    const [card, setCard] = useState(data);
    const [responded, setresponded] = useState(false);

    const handleClick = (index, answer) => {
        console.log(index + ": " + answer);
        data.responded = true;
        data.indexResponse = index;
        if (answer === data.correctAnswer) {
            data.correct = true;
        } else {
            data.correct = false;
        }
        setCard(data);
        setresponded(true);

        onResponse(data.correct)
    }

    const getClass = (index, answer) => {
        if (responded) {
            if (answer === card.correctAnswer) {
                return "btn btn-success btn-lg btn-block"
            } else if (card.correct === false && index === card.indexResponse) {
                return "btn btn-danger btn-lg btn-block"
            } else {
                return "btn btn-secondary btn-lg btn-block"
            }
        } else {
            return "btn btn-primary btn-lg btn-block"
        }
    }

    let buttons = card.answers.map((answer, index) =>
        <button type="button" name="" id="" class={getClass(index, answer)} key={index} onClick={() => { handleClick(index, answer) }} disabled={card.responded}>{he.decode(answer)}</button>
    );
    return (
        <div class="card h-100 d-flex flex-column">
            <div class="card-body">
                <h4 class="card-title">{he.decode(card.question)}</h4>
                {buttons}


            </div>
            <div class="card-footer align-self-end w-100">
                {card.getDifficulty()}
                </div>

        </div>
    )
}

export default Card
