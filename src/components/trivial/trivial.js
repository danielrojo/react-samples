import React, { useState, useEffect } from 'react'
import CardModel from '../../model/cardmodel';
import Card from './card';

function Trivial() {

    const [url, setUrl] = useState('https://opentdb.com/api.php?amount=10')
    const [cards, setCards] = useState([])
    const [score, setScore] = useState(0)

    const doRequest = () => {
        console.log('url: ' + JSON.stringify(url));
        fetch(url)
            .then(response => response.json())
            .then(data => {
                let tmp = []
                for (const jsonCard of data.results) {
                    tmp = [...tmp, new CardModel(jsonCard)]
                }
                setCards(tmp);
                console.log(JSON.stringify(data.results));
            })

    }

    useEffect(doRequest, [])

    const handleResponse = (correct, difficulty) => {
        if(correct) {
            setScore(score + difficulty)
        }else {
            setScore(score - 1)
        }
    }

    let myCards = <div>Loading...</div>

    if (cards.length > 0) {
        myCards = cards.map((card, index) =>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 my-3" key={index}>
                <Card data={card} onResponse={(correct)=>{handleResponse(correct,card.difficulty)}} />
            </div>
        )
    }

    return (


        <div class="container">
            <h1><span class="badge badge-primary">{score}</span></h1>
            <div class="row">
                {myCards}
            </div>

        </div>
    )
}

export default Trivial
