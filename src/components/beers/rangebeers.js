import React, {useState} from 'react';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';

function RangeBeers(props) {
    const [value, setValue] = useState(props.defaultRange);

    const handleRange = (value) => {
        setValue(value);
        props.range(value)
    }

    return (
        <div>
            <Typography id="range-slider" gutterBottom>
                Alcohol
                    </Typography>
            <Slider
                min={0}
                max={60}
                step={0.1}
                value={value}
                onChange={(event, value) => { handleRange(value) }}
                valueLabelDisplay="auto"
                aria-labelledby="range-slider"
            />
        </div>
    )
}

export default RangeBeers
