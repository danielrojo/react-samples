import React, { Component } from 'react';
import BeersList from './beerslist';
import RangeBeers from './rangebeers';

export class Beers extends Component {

    constructor(props) {
        super(props)

        this.state = {
            data: this.props.beers,
            value: this.props.range
        }
    }

    doRequest() {
        let url = 'https://api.punkapi.com/v2/beers';

        fetch(url)
            .then(response => response.json())
            .then(data => {
                this.setState({ data: data });
                console.log(data);
                this.props.updateBeers(data);
            })

    }

    componentDidMount() {
        console.log('componentDidMount(): Beers');
        if(this.state.data.length === 0){
            this.doRequest();
        }
    }

    handleRange(value) {
        console.log(JSON.stringify(value));
        this.setState({value: value});
        this.props.updateRange(value);
    }
    

    render() {
        
        return (
            <div class="container">
                <RangeBeers range = {(value)=>this.handleRange(value)} defaultRange={this.state.value}/>
                <BeersList data={this.state.data} value={this.state.value}/>
            </div>
        )
    }
}

export default Beers
