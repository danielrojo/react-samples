import React from 'react'

function BeersList({data, value}) {

    let content = <div></div>
    if (data.length > 0) {
        content = data
            .filter((beer) => beer.abv >= value[0] && beer.abv <= value[1])
            .map((beer, index) =>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 my-4" key={index}>
                    <div class="card h-100" >
                        <img class="card-img-top d-block mx-auto mt-4 w-25" src={beer.image_url} alt="" />
                        <div class="card-body">
                            <h4 class="card-title">{beer.name}</h4>
                            <p class="card-text">{beer.tagline}</p>
                            <p class="card-text">{beer.abv}</p>
                        </div>
                    </div>
                </div>
            );
    }

    return (

        <div class="row">
            {content}
        </div>
    )
}

export default BeersList
