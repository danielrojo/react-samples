import React, {Fragment, useState, useEffect} from 'react';
import Spinner from 'react-bootstrap/Spinner';
import SwCard from './swcard';
import PropTypes from 'prop-types';

function SwPage({url}) {

    const [people, setPeople] = useState([])

    const doRequest = () => {
        fetch(url)
            .then(response => response.json())
            .then(data => {
                setPeople([...data.results]);
                console.log(JSON.stringify(data.results));
            })

    }

    useEffect(doRequest, [url])

    let content = <div></div>;
    let cards = <div></div>;
    cards = people
    .map((character, index) =>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4" key={index}>
            <SwCard character={character} />
        </div>
    );
    if (people.length > 0) {
        content = cards
    } else {
        <Spinner animation="border" role="status">
            <span className="sr-only">Loading...</span>
        </Spinner>
    }
    return (
        <Fragment>
            {content}
        </Fragment>
    )
}

SwPage.propTypes = {
    url: PropTypes.string.isRequired
  }

export default SwPage
