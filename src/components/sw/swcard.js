import React, {useEffect} from 'react'

function SwCard(props) {

    const doSomething = () => {
        return () => {
            console.log('ComponentWiilUnmount Hook: SW');
        }
    }

    useEffect(doSomething,[])

    return (
        <div class="card m-3">
            <div class="card-body">
                <h4 class="card-title">{props.character.name}</h4>
                <p class="card-text">{props.character.height}</p>
                <p class="card-text">{props.character.mass}</p>
            </div>
        </div>
    )
}

export default SwCard
