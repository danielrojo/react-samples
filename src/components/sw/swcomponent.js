import React, { useState, useEffect } from 'react';

import Pagination from 'react-bootstrap/Pagination';
import SwPage from './swpage';

function SwComponent(props) {

    const [active, setActive] = useState(props.data.active)
    const [total, setTotal] = useState(0);
    const [url, setUrl] = useState(props.data.url)

    const doRequest = () => {
        console.log('url: '+JSON.stringify(url));
        fetch(url)
            .then(response => response.json())
            .then(data => {
                setTotal(data.count);
                console.log(JSON.stringify(data.results));
            })

    }

    useEffect(doRequest, [])

    let items = [];

    const setClick = (number) => {
        console.log('number: ' + number);
        setActive(number)
        setUrl('https://swapi.dev/api/people/?page='+number);
        props.updateSw({active: number})
    };

    for (let number = 1; number <= Math.ceil(total/10); number++) {
        items.push(
            <Pagination.Item key={number} active={number === active} onClick={()=>{setClick(number)}}>
                {number}
            </Pagination.Item>,
        );
    }

    const paginationBasic = (
        <div>
            <Pagination>{items}</Pagination>
            <br />
        </div>
    );


    return (

        <div class="container">
            <div class="row">
                <SwPage url={url}/>
            </div>
            {paginationBasic}
        </div>

    )
}

export default SwComponent
