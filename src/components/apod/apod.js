import React, { Component } from 'react';
import "react-datepicker/dist/react-datepicker.css";
import ShowApod from './showapod';
import DateApod from './dateapod';

export class Apod extends Component {

    constructor(props) {
        super(props)

        this.state = {
            data: {},
            date: this.props.date
        }
    }

    componentWillUnmount() {
        console.log('componentWillUnmount: Apod');
    }

    handleChange(date) {
        this.props.updateDate(date);
    }


    render() {
        return (
            <div class="container">
                <DateApod onData={(data)=>{this.setState({data: data})}} date={this.state.date} onDateChange={(date)=>{this.handleChange(date)}}/>
                <ShowApod data={this.state.data}/>
            </div>
        )
    }
}

export default Apod
