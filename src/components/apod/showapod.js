import React, { Component } from 'react';
import ReactPlayer from 'react-player';

export class ShowApod extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             
        }
    }
    
    render() {
        let content = <div></div>;
        if(this.props.data.media_type === 'image') {
            content = <img src={this.props.data.url} className="img-fluid d-block mx-auto" alt=""></img>
        }else if(this.props.data.media_type === 'video') {
            content = <ReactPlayer className="d-block mx-auto" url={this.props.data.url}/>
        }
        return (
                <div className="jumbotron">
                    <h1 className="display-3">{this.props.data.title}</h1>
                    <p className="lead">{this.props.data.date}</p>
                    {content}
                    <hr className="my-2" />
                    <p>{this.props.data.explanation}</p>
                    <p className="lead">
                        <a className="btn btn-primary btn-lg" href={this.props.data.hdurl} role="button">Imagen HD</a>
                    </p>
                </div>
        )
    }
}

export default ShowApod
