import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import moment from 'moment';

export class DateApod extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
            date: this.props.date
        }
    }

    doRequest(date) {
        let url = 'https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY';
        console.log('Date: ' + JSON.stringify(date));
        if(date === undefined) {

        } else {
            const myDate = moment(date).format('YYYY-MM-DD')
            url = url + '&date=' + myDate;
        }

        fetch(url)
        .then(response => response.json())
        .then(data => {
            console.log(JSON.stringify(data));
            this.props.onData(data);
            // this.setState({ data: data });
        })

    }

    componentDidMount() {
        console.log('componentDidMount(): Apod');
        this.doRequest(this.state.date)
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevState.date !== this.state.date) {
            this.doRequest(this.state.date);
        }
    }

    handleChange(date) {
        this.setState({date: date});
        this.props.onDateChange(date);
    }
    
    render() {
        return (
                <DatePicker selected={this.state.date} onChange={date => {this.handleChange(date)}} />

        )
    }
}

export default DateApod
