import Apod from "../components/apod/apod";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return {
    date: state.apod.date,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateDate: date => {
      dispatch({ type: "UPDATE_DATE", payload: { date } });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Apod);