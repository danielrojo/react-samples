import SwComponent from "../components/sw/swcomponent";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return {
    data: {active: state.sw.active, url: 'https://swapi.dev/api/people/?page='+state.sw.active},
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateSw: data => {
      dispatch({ type: "UPDATE_SW", payload: { data } });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SwComponent);