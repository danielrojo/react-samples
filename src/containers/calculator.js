import Calculator from "../components/calculator/calculator";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return {
    data: state.calculator,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateCalculator: calculator => {
      dispatch({ type: "UPDATE_CALCULATOR", payload: { calculator } });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Calculator);