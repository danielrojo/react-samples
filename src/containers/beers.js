import Beers from "../components/beers/beers";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return {
    range: state.beers.range,
    beers: state.beers.listBeers
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateRange: range => {
      dispatch({ type: "UPDATE_RANGE", payload: { range: range } });
    },
    updateBeers: myBeers => {
        dispatch({ type: "UPDATE_LIST_BEERS", payload: { beers: myBeers } });
      }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Beers);