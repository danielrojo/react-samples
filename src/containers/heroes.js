import Heroes from "../components/heroes/heroes";
import { connect } from "react-redux";

const mapStateToProps = state => {
  return {
    heroes: state.heroes.heroesList,
    heroForm: state.heroes.heroForm
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addHero: myHero => {
      dispatch({ type: "ADD_HERO", payload: { hero: myHero } });
    },
    updateHeroForm: myHero => {
      dispatch({ type: "UPDATE_HERO_FORM", payload: { hero: myHero } });
    }

  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Heroes);