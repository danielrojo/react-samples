import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

// Components
import Trivial from './components/trivial/trivial';

// Containers
import Beers from './containers/beers';
import Heroes from './containers/heroes';
import Apod from './containers/apod';
import Calculator from './containers/calculator';
import SwComponent from './containers/swcomponent';

function App() {
  return (
    <div className="App">
      <Router>
        <div>
          <nav className="navbar navbar-expand-sm navbar-dark bg-primary">
            <a className="navbar-brand" href="#">React Samples</a>
            <button className="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
              aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="collapsibleNavId">
              <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                <li className="nav-item">
                  <Link className="nav-link" to="/calculator">Calculadora</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/heroes">Héroes</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/apod">Apod</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/beers">Cervezas</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/sw">Star Wars</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/trivial">Trivial</Link>
                </li>
              </ul>
            </div>
          </nav>

          {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
          <Switch>
            <Route path="/calculator">
              <Calculator />
            </Route>
            <Route path="/heroes">
              <Heroes />
            </Route>
            <Route path="/apod">
              <Apod />
            </Route>
            <Route path="/beers">
              <Beers />
            </Route>
            <Route path="/sw">
              <SwComponent />
            </Route>
            <Route path="/trivial">
              <Trivial/>
            </Route>
            <Route path="*">
              <h1>Página 404</h1>
            </Route>
          </Switch>
        </div>
      </Router>

    </div>
  );
}

export default App;
